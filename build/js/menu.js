var menu = document.getElementById('header');

var coordsEmergenceMenu = document.querySelector(".about-us").getBoundingClientRect().top + window.pageYOffset;


window.onscroll = function() {
  if (menu.classList.contains('fixed') && window.pageYOffset < coordsEmergenceMenu) {
    menu.classList.remove('fixed');
  } else if (window.pageYOffset > coordsEmergenceMenu) {
    menu.classList.add('fixed');
  }
};




var burgerButton = document.querySelector(".navigation__toggler");

burgerButton.onclick = function() {
  menu.classList.toggle('show-menu');
};

// scroll window
window.smoothScroll = function(target) {
  if (menu.classList.contains('show-menu')) {
    menu.classList.remove('show-menu');
  }

  var scrollContainer = target;
  do { //find scroll container
      scrollContainer = scrollContainer.parentNode;
      if (!scrollContainer) return;
      scrollContainer.scrollTop += 1;
  } while (scrollContainer.scrollTop == 0);
  
  var targetY = 0;
  do { //find the top of target relatively to the container
      if (target == scrollContainer) break;
      targetY += target.offsetTop;
  } while (target == target.offsetParent);
  
  scroll = function(c, a, b, i) {
      i++; if (i > 30) return;
      c.scrollTop = a + (b - a - 99) / 30 * i;
      setTimeout(function(){ scroll(c, a, b, i); }, 20);
  }
  // start scrolling
  scroll(scrollContainer, scrollContainer.scrollTop, targetY, 0);
}