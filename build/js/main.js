const servBranding = new CountUp('servBranding', 0, 80, 0, 5);
const servDesign = new CountUp('servDesign', 0, 75, 0, 5);
const servUI = new CountUp('servUI', 0, 60, 0, 5);

startNumbers(servBranding);
startNumbers(servDesign);
startNumbers(servUI);

const teamBranding = new CountUp('teamBranding', 0, 80, 0, 5);
const teamDesign = new CountUp('teamDesign', 0, 65, 0, 5);
const teamUI = new CountUp('teamUI', 0, 75, 0, 5);

startNumbers(teamBranding);
startNumbers(teamDesign);
startNumbers(teamUI);

function startNumbers(number) {
    if(!number.error) {
        number.start();
    } else {
        console.log(number.error);
    }
    // number.start();
}